import { createClient, RedisClient } from 'redis';

const adicionarFila = async (pRedis: RedisClient, pFila: string, pValue: string): Promise<void> => {
  try {
    pRedis.lpush(pFila, pValue, (err, reply) => {
      console.log('Item adicionado a fila.');
      return Promise.resolve();
    });
  } catch (error) {
    return Promise.reject(error);
  }
};

const listarTodaFila = (pRedis: RedisClient, pFila: string): Promise<string[]> => {
  return new Promise((resolve, reject) => {
    try {
      pRedis.lrange(pFila, 0, -1, (err, reply) => {
        return resolve(reply);
      });
    } catch (error) {
      return reject(error);
    }
  });
};

const listarProximoFila = (pRedis: RedisClient, pFila: string): Promise<string | null> => {
  return new Promise((resolve, reject) => {
    try {
      pRedis.lrange(pFila, 0, 0, (err, reply) => {
        if (reply.length === 0) {
          return resolve(null);
        }
        return resolve(reply[0]);
      });
    } catch (error) {
      return reject(error);
    }
  });
};

const removerPrimeiroFila = (pRedis: RedisClient, pFila: string): Promise<void> => {
  try {
    pRedis.lpop(pFila);
    return Promise.resolve();
  } catch (error) {
    return Promise.reject(error);
  }
};

(async () => {
  const client: RedisClient = createClient({ host: '127.0.0.1', port: 6379 });

  const fila: string = 'mylist';
  client.del(fila);

  adicionarFila(client, fila, 'value1');
  adicionarFila(client, fila, 'value2');
  adicionarFila(client, fila, 'value3');
  adicionarFila(client, fila, 'value4');
  adicionarFila(client, fila, 'value5');

  console.log(await listarTodaFila(client, fila));
  console.log(await listarProximoFila(client, fila));

  await removerPrimeiroFila(client, fila);

  console.log(await listarTodaFila(client, fila));
  console.log(await listarProximoFila(client, fila));

  client.quit();
})();
