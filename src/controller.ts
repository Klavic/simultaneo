import { Request, Response } from 'express';

import redisHelper from './redisHelper';

class FilaController {
  constructor() { }

  public fila() {
    return async (req: Request, res: Response) => {
      const { documento, usuario }: { documento: string, usuario: string } = req.body;

      /**
       * Adiciona o usuário na fila do documento.
       */
      await redisHelper.adicionarFila(documento, usuario);

      /**
       * Espera a vez do usuário a ser processado.
       */
      let sair: boolean = false;
      do {
        const minhaVez: boolean = await redisHelper.listarProximoFila(documento) === usuario;
        if (minhaVez === true) {
          sair = true;
        } else {
          await sleep(300);
        }
      } while (sair === false);

      /**
       * Realiza o processamento do documento.
       */
      console.log('Processando documento: ', documento);
      await sleep(1000);

      console.log(`Documento ${documento} finalizado.`);

      /**
       * No final de todo o processo, remover o usuário da fila.
       * No caso do NPAPER apos finalizar a assinatura no ultimo passo.
       */
      await redisHelper.removerPrimeiroFila(documento);
      return res.status(200).json({ message: `Documento ${documento} processado com sucesso.` });
    };
  }
}

function sleep(pTime: number): Promise<void> {
  return new Promise((resolve, reject) => {
    try {
      setInterval(() => {
        return resolve();
      }, pTime);
    } catch (error) {
      return reject(error);
    }
  });
}

export default new FilaController();
