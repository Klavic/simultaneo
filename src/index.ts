import * as Express from 'express';
import * as http from 'http';

import * as Cors from 'cors';
import * as BodyParser from 'body-parser';
import * as Helmet from 'helmet';

import controller from './controller';

const server = Express();

// Middelwares
server.use((req, res, next) => {
  res.header('Content-Type', 'application/json');
  next();
});
server.use(BodyParser.json());
server.use(BodyParser.urlencoded({
  extended: true,
}));
server.use(Cors());
server.use(Helmet());

// Routes
server.get('/', (req, res) => {
  return res.status(200).json('Funcionando');
});
server.post('/fila', controller.fila());

// Start
http.createServer(server).listen(3000, () => {
  console.log('Servidor roando na porta 3000');
});
