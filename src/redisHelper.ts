import { createClient, RedisClient } from 'redis';

class Redis {
  public cliente: RedisClient;

  constructor() {
    this.cliente = createClient({ host: '127.0.0.1', port: 6379 });
  }

  public async adicionarFila(pFila: string, pValue: string): Promise<void> {
    try {
      this.cliente.lpush(pFila, pValue, (err, reply) => {
        console.log('Item adicionado a fila.');
        return Promise.resolve();
      });
    } catch (error) {
      return Promise.reject(error);
    }
  }

  public async listarTodaFila(pFila: string): Promise<string[]> {
    return new Promise((resolve, reject) => {
      try {
        this.cliente.lrange(pFila, 0, -1, (err, reply) => {
          return resolve(reply);
        });
      } catch (error) {
        return reject(error);
      }
    });
  }

  public async listarProximoFila(pFila: string): Promise<string | null> {
    return new Promise((resolve, reject) => {
      try {
        this.cliente.lrange(pFila, 0, 0, (err, reply) => {
          if (reply.length === 0) {
            return resolve(null);
          }
          return resolve(reply[0]);
        });
      } catch (error) {
        return reject(error);
      }
    });
  }

  public async removerPrimeiroFila(pFila: string): Promise<void> {
    try {
      this.cliente.lpop(pFila);
      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  }
}

export default new Redis();
